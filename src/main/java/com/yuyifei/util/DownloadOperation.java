package com.yuyifei.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

import com.yuyifei.classify.ClassifyCommon;
import com.yuyifei.common.Common;

public class DownloadOperation {
	/**
	 * @param storeDir: the Dir for store apk file
	 * @param apkName: apk name
	 * @param url: download url
	 * @return: file --- the downloaded apk file
	 */
	public static File downloadFromUrl(String storeDir, String apkName, String url)
	{
		System.out.println(storeDir + ":" + apkName + ":" + url);
		
		//download apk file
		URL httpUrl = null;
		try {
			httpUrl = new URL(url);
		} catch (MalformedURLException e) {
			System.err.println("create URL failed.");
			e.printStackTrace();
		}
		
		File apkFile = new File (storeDir + "/" + apkName);
		if (apkFile.exists())
		{
			System.out.println("Apk file exiist, detele it.");
			return apkFile;
			//apkFile.delete();
		}
		
		try {
			FileUtils.copyURLToFile(httpUrl, apkFile);
		} catch (IOException e) {
			System.err.println("Dowload failed:" + url);
			e.printStackTrace();
		}
		
		return apkFile;
	}
	
	/**
	 * @param marketName: market name
	 * @param keyWord: apk file keyword given by user
	 * @param fileName: file name 
	 * @param url: download url
	 */
	public static void downloadFromUrl(String marketName, String keyWord, String fileName, String url)
	{
			URL httpurl = null;
			try {
				System.out.println("yuyifei:" + url);
				//httpurl = new URL("http://gdown.baidu.com/data/wisegame/34c22e79d212844d/baiduyunjishiben.apk");
				httpurl =  new URL(url);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//creat dir
			File file1 = new File(Common.getApkDir() + keyWord + "/" + marketName);
			file1.mkdirs();
			
			//download it 
			File file2 = new File(Common.getApkDir() + keyWord + "/" + marketName + "/" + fileName);
			try {
				FileUtils.copyURLToFile(httpurl, file2);
				System.out.println(fileName + ":" + "Download compelete!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
