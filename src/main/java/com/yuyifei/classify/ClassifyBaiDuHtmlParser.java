package com.yuyifei.classify;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.yuyifei.common.Html;
import com.yuyifei.util.DownloadOperation;

public class ClassifyBaiDuHtmlParser extends ClassifyParser
{
	@Override
	public void parserHtml() 
	{
		String url = ClassifyCommon.getBaiDuListrl();
		String html = Html.getHtmlFromUrl(url);
		Document doc = Jsoup.parse(html);
		Elements divs = doc.select("div.cate-head");
		//System.out.println(divs.size());
		
		for (Element div : divs)
		{
			Element a = div.select("a").first();
			//System.out.println(a.text() + ":" + a.attr("href"));
			String newUrl = "http://shouji.baidu.com" + a.attr("href");
			String classifyName = a.text();
			findDownloadUrl(classifyName, newUrl);
		}
	}

	private void findDownloadUrl(String classifyName, String newUrl) 
	{	
		//System.out.println(classifyName + ":" + newUrl);//debug it
		
		int count = 0;
		String html = Html.getHtmlFromUrl(newUrl);
		Document doc = Jsoup.parse(html);
		//Element ul = doc.select("div.list-bd > ul").first();
		//System.out.println(ul.select("li").size());
		Elements lis = doc.select("div.list-bd > ul").first().select("li");
		
		for (Element li : lis)
		{
			Element a = li.select("div.app-box > a").first();
			Element p = li.select("div.app-meta").select("p.name").first();
			System.out.println(a.attr("href"));
			System.out.println(p.text());
			
			String url = "http://shouji.baidu.com" + a.attr("href");
			String apkName = p.text();
			
			downloadApk(classifyName, apkName, url);
			
			//count download number
			++count;
			if (count >= ClassifyCommon.getNumber())
			{
				System.out.println("download apk file number: " + count);//downloadApk
				break;
			}
		}
	}

	private void downloadApk(String classifyName, String apkName, String url) 
	{
		//System.out.println(url);
		String html = Html.getHtmlFromUrl(url);
		Document doc = Jsoup.parse(html);
		Element a = doc.select("div.area-download").select("a.apk").first();
		//System.out.println(a.attr("href"));
		
		String downloadUrl = a.attr("href");
		String storeDir = ClassifyCommon.getRootDir() + "/"
						+ ClassifyCommon.getBaiDuMarketName() +  "/"
						+ classifyName;
		
		//download apk file
		File apkFile = DownloadOperation.downloadFromUrl(storeDir, apkName + ".apk", downloadUrl);
		
		//store apk info
		ApkInformation apk = new ApkInformation();
		apk.setApkMarketName(ClassifyCommon.getJiFengMarketName());
		apk.setApkName(apkName);
		apk.setDownloadUrl(downloadUrl);
		apk.setStorePath(apkFile.getAbsolutePath());
		ClassifyCommon.apkList.add(apk);
	}
}
