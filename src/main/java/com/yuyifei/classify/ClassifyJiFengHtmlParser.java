package com.yuyifei.classify;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.yuyifei.common.Html;
import com.yuyifei.util.DownloadOperation;

public class ClassifyJiFengHtmlParser extends ClassifyParser
{
	@Override
	public void parserHtml() 
	{
		String url = ClassifyCommon.getJiFengListUrl();
		String html = Html.getHtmlFromUrl(url);
		Document doc = Jsoup.parse(html);
		Elements as = doc.select("div.lp-left > div").first().select("a");
		//System.out.println(doc.select("div.lp-left > div").first().select("a").size());
		
		for (Element a : as)
		{
			//System.out.println(a.text());
			String classifyName = a.text();
			String newUrl = "http://apk.gfan.com" + a.attr("href");
			findDownloadUrl(classifyName, newUrl);
			
		}
	}

	private void findDownloadUrl(String classifyName, String newUrl) 
	{
		int count = 0;
		String html = Html.getHtmlFromUrl(newUrl);
		Document doc = Jsoup.parse(html);
		Elements lis = doc.select("div.list-page > ul").first().select("li");
		//System.out.println(lis.size());
		for (Element li : lis)
		{
			
			//System.out.println(li.select("span.apphot-tit > a").text());//http://apk.gfan.com/
			String url = "http://apk.gfan.com" + li.select("span.apphot-tit > a").attr("href");
			String apkName = li.select("span.apphot-tit > a").text();
			DownloadApk(classifyName, apkName, url);
			
			//count download number
			++count;
			if (count >= ClassifyCommon.getNumber())
			{
				System.out.println("download apk file number: " + count);//downloadApk
				break;
			}
		}
	}

	private void DownloadApk(String classifyName, String apkName, String url) 
	{
		String html = Html.getHtmlFromUrl(url);
		Document doc = Jsoup.parse(html);
		
		//System.out.println(doc.getElementById("computerLoad").attr("href"));
		String downloadUrl = doc.getElementById("computerLoad").attr("href");
		String storeDir = ClassifyCommon.getRootDir() + "/"
						+ ClassifyCommon.getJiFengMarketName() + "/"
						+ classifyName;
		
		//download apk file
		File apkFile = DownloadOperation.downloadFromUrl(storeDir, apkName + ".apk", downloadUrl);
		
		//store apk info
		ApkInformation apk = new ApkInformation();
		apk.setApkMarketName(ClassifyCommon.getJiFengMarketName());
		apk.setApkName(apkName);
		apk.setDownloadUrl(downloadUrl);
		apk.setStorePath(apkFile.getAbsolutePath());
		ClassifyCommon.apkList.add(apk);
	}
}
